# sysrepo packaging tests

This repo contains CI for upstream packages of following projects:

* [libyang](https://github.com/CESNET/libyang)
* [libnetconf2](https://github.com/CESNET/libnetconf2)
* [sysrepo](https://github.com/sysrepo/sysrepo)
* [netopeer](https://github.com/CESNET/netopeer2)

[apkg](https://apkg.readthedocs.io/) is used to build and test packages
directly from upstream sources.
