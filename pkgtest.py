#!/usr/bin/env python3

import os
from pathlib import Path
import re

from apkg.log import getLogger
from apkg.util.run import run, cd
from apkg.util.git import git


# default repo branch to clone
PKGTEST_BRANCH = os.getenv('PKGTEST_BRANCH', 'devel')

log = getLogger(__name__)


def git_repo_name(url):
    _, _, repo = url.rpartition('/')
    return Path(repo).with_suffix('').name


class Project:
    def __init__(self, url, branch=None):
        self.url = url
        self.branch = branch or PKGTEST_BRANCH
        self.name = git_repo_name(url)


PROJECTS = [
    #Project('https://github.com/jruzicka-nic/libyang.git', branch='pkgtest'),
    Project('https://github.com/CESNET/libyang.git'),
    Project('https://github.com/CESNET/libnetconf2.git'),
    Project('https://github.com/sysrepo/sysrepo.git'),
    Project('https://github.com/CESNET/netopeer2.git'),
]


def run_pkgtests():
    for proj in PROJECTS:
        p = Path(proj.name)
        log.info('PKGTESTS for %s: %s' % (proj.name, proj.url))
        if not p.exists():
            git('clone', '-b', proj.branch, proj.url, tee=True)
        with cd(p):
            run('apkg', 'install', '--build-dep', tee=True)
            run('apkg', 'test', '--test-dep', tee=True)


if __name__ == '__main__':
    run_pkgtests()
